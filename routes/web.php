<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

/** MANY TASKS ROUTES **/
//Show tasks
Route::get('/', 'TaskController@index')->middleware('auth')->name('home');

//Show deleted tasks
Route::get('/deleted', 'TaskController@indexDeleted')->middleware('auth')->name('index.deleted');

//Permanently remove all user's soft deleted task
Route::delete('/emptyBin', 'TaskController@emptyBin')->middleware('auth')->name('index.deleted.emptybin');
/********************************************************************************/


/** SINGLE TASK ROUTES **/
//New Task
Route::post('/', 'TaskController@store')->middleware('auth')->name('task.store');

//Show task
Route::get('/{task}', 'TaskController@show')->middleware('auth')->name('task.show');

//Edit Task
Route::put('/{task}', 'TaskController@update')->middleware('auth')->name('task.update');

//Toggle task complete
Route::put('/{task}/toggle-complete', 'TaskController@toggleComplete')->middleware('auth')->name('task.toggle-complete');

//Soft delete task, or permanently remove it if it is already
Route::delete('/{task}', 'TaskController@destroy')->middleware('auth')->name('task.delete');

//Restore soft-deleted task
Route::put('/{task}/restore', 'TaskController@restore')->middleware('auth')->name('task.restore');
/********************************************************************************/

@extends('layouts.app')

@section('content')

<div class="row text-center">

    <!-- To-do section -->

    <div class="col-md-6 mb-4">
        <p class="title">
            To Do <i class="fas fa-clipboard" style="color:#4980af"></i>
        </p>

        @forelse ($todos as $todo)
            <div class="card mb-2 shadow">
                <div class="card-body">
                    <div class="row">

                        <div class="col-4 offset-4">
                            {{$todo->title}}
                        </div>

                        <div class="col-2 offset-2 reveal">
                            <span class="badge badge-primary">
                                Open Task <i class="fa fa-align-justify ml-1"></i>
                            </span>
                        </div>

                        <a class="stretched-link" href="{{$todo->link()}}"></a>
                    </div>
                </div>
                <div class="card-footer">
                    Created: {{ $todo->created_at->diffForHumans() }}

                    @if($todo->deadline)
                    /
                    Deadline: {{ $todo->deadline->diffForHumans() }}
                    @endif
                </div>
            </div>
        @empty
            <p>You don't have to-do tasks.</p>
        @endforelse
    </div>

    <!-- Completed section -->

    <div class="col-md-6">
        <p class="title">
            Completed <i class="fas fa-check-circle" style="color:green"></i>
        </p>

        @forelse ($completed as $completedTask)
            <div class="card mb-2 shadow">
                <div class="card-body">
                    <div class="row">
                        <div class="col-4 offset-4">
                            {{$completedTask->title}}
                        </div>

                        <div class="col-2 offset-2 reveal">
                            <span class="badge badge-primary">
                                Open Task <i class="fa fa-align-justify ml-1"></i>
                            </span>
                        </div>

                        <a class="stretched-link" href="{{ $completedTask->link() }}"></a>
                    </div>
                </div>
                <div class="card-footer">
                    Created: {{ $completedTask->created_at->diffForHumans() }}
                    /
                    Completed: {{ $completedTask->completed_at->diffForHumans() }}
                </div>
            </div>
        @empty
            <p>You don't have completed tasks.</p>
        @endforelse
    </div>
</div>


@endsection

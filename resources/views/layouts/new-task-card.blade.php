<!-- work-around -->
<!-- TODO: find a way to align the cards while keeping the "Todo" and "Completed" headers -->
<p class="title" style="visibility:hidden">
    a
</p>

<div class="card shadow mb-4">
    <div class="card-header">
        New Task
    </div>
    <div class="card-body">
        <form name="createForm" method="POST" action="{{route('task.store')}}">
            @csrf

            <div class="form-group">

                <label for="title">Title</label>
                <input type="text" class="form-control @error('title') is-invalid @enderror" name="title" aria-describedby="title" placeholder="Enter task name">
                @error('title')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
                <small class="form-text text-muted">This will be shown on the tasks list.</small>
            </div>

            <div class="form-group">
                <label for="body">Task body (Optional)</label>
                <textarea class="form-control" name="body" rows="10" placeholder="Enter task content">{{ old('body') }}</textarea>
            </div>

            <div class="form-group date">
                <label for="deadline">Deadline (Optional)</label>
                <input class="form-control" type="text" name="deadline" value=""/>
                <span class="add-on"></span>
            </div>

            <div class="form-check mb-3">
                <input type="checkbox" class="form-check-input" name="important" {{ old('important') ? 'checked' : '' }}>
                <label class="form-check-label" for="important">
                    High priority task
                    <span data-toggle="tooltip" title="Check if the task is important." data-placement="right">(?)</span>
                </label>
            </div>



            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>

<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}"></script>

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/navbar.css') }}" rel="stylesheet">
        <!-- Icons -->
        <script src="https://kit.fontawesome.com/beeb1623f0.js" crossorigin="anonymous"></script>

        @yield('head')

    </head>
    <body>
        @include('layouts.navbar')

        <div class="container-fluid mt-4">

            <!-- On xl devices -->
            <!-- New Task | Tasks | Profile -->

            <!-- On < xl devices -->
            <!-- Tasks -->
            <!-- New task -->
            <!-- Profile -->
            <div class="row">
                @auth
                    <div class="col-xl-2 col-lg-12 order-2 order-xl-first text-center">
                        @include('layouts.new-task-card')
                    </div>
                    <div class="col-xl-8 col-lg-12 order-1">
                        @yield('content')
                    </div>
                    <div class="col-xl-2 col-lg-12 order-3 text-center">
                        @include('layouts.profile-card')
                    </div>
                @else
                    <div class="offset-md-2 col-md-8">
                        @yield('content')
                    </div>
                @endauth
        </div>

    </body>
</html>

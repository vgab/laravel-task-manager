<!-- work-around -->
<!-- TODO: find a way to align the cards while keeping the "Todo" and "Completed" headers -->
<p class="title" style="visibility:hidden">
    a
</p>

<div class="card mb-2 shadow side-card">
    <div class="card-header">
        Profile
    </div>
    <div class="card-body">
        <img src="{{ asset('img/profile.png') }}" width="100" height="100" alt="">
        <p>Name: {{ Auth::user()->name }}</p>
        <p>Email: {{ Auth::user()->email }}</p>
    </div>
</div>

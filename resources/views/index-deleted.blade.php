@extends('layouts.app')

@section('head')

<link rel="stylesheet" href="{{asset('css/index-deleted.css')}}">

@section('content')


<div class="row">

    <!-- TASK HEADER -->
    <div class="col-6">
        <p class="title">
            Deleted tasks
        </p>
    </div>

    <!-- "EMTPY BIN" BUTTON -->
    <div class="offset-4 col-2">
        <p class="title">

            <form method="POST" action="{{route('index.deleted.emptybin')}}">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-sm btn-outline-danger float-right">
                    Empty bin
                </button>
            </form>

        </p>
    </div>
</div>


<div class="row">



@forelse($deletedTasks->split(ceil($deletedTasks->count()/2)) as $row)

    @foreach($row as $item)
        <div class="col-6 text-center">
            <div class="card mb-2 shadow">
                <div class="card-body">
                    <div class="row">

                        <div class="col-4 offset-4">
                            {{$item->title}}
                        </div>

                        <!-- Restore, Delete -->
                        <div class="col-4 reveal">

                            <div class="d-none">
                                <form id="restoreForm{{$item->id}}" action="{{route('task.restore', $item->id)}}" method="POST">
                                    @csrf
                                    @method('PUT')
                                </form>
                                <form id="deleteForm{{$item->id}}" action="{{route('task.delete', $item->id)}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                </form>
                            </div>

                            <span class="badge badge-warning" onclick="restoreForm{{$item->id}}.submit()">
                                Restore <i class="fa fa-history"></i>
                            </span>
                            <span class="badge badge-danger" onclick="deleteForm{{$item->id}}.submit()">
                                Remove <i class="fa fa-trash"></i>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    Created: {{ $item->created_at->diffForHumans() }}
                </div>
            </div>
        </div>
    @endforeach

@empty
    <div class="col-6">
        <p>You don't have deleted tasks.</p>
    </div>
@endforelse

</div>

@endsection

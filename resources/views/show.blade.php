@extends('layouts.app')

@section('content')


<div class="row">

    <!-- TASK HEADER -->
    <div class="col-6">
        <p class="title">
            Task status:
            @if($task->completed_at)
                <i class="fas fa-check-circle" style="color:green"></i>
            @else
                <i class="fas fa-clipboard" style="color:#4980af"></i>
            @endif
        </p>
    </div>

    <!-- "COMPLETED" BUTTON -->
    <div class="offset-2 col-3 text-right">
        <p class="title">

            <form method="POST" action="{{route('task.toggle-complete', $task)}}">
                @csrf
                @method('PUT')

                <button type="submit" class="btn btn-sm {{$task->completed_at ? 'btn-outline-warning' : 'btn-outline-success'}}">
                    @if($task->completed_at)
                        Mark "To-do"
                    @else
                        Mark "Completed"
                    @endif
                </button>
            </form>

        </p>
    </div>

    <!-- "DELETE" BUTTON -->
    <div class="col-1">
        <p class="title">
            <form method="POST" action="{{route('task.delete', $task->id)}}">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-sm btn-outline-danger">
                    Delete
                </button>
            </form>
        </p>
    </div>
</div>


<div class="card mb-2 shadow">

    <div class="card-body">
        <form method="POST" action="{{route('task.update', $task->id)}}">
            @csrf
            @method('PUT')

            <div class="form-group">
                <input type="text" name="newTitle" class="form-control @error('newTitle') is-invalid @enderror" value="{{$task->title}}">
                @error('newTitle')
                    <div class="invalid-feedback">The title field is required.</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Body</label>
                <textarea class="form-control" name="newBody" rows="10">{{ $task->body }}</textarea>
            </div>

            <p>
                Created: {{ $task->created_at->diffForHumans() }}
            </p>

            @if($task->completed_at)
                <p>
                    Completed: {{ $task->completed_at->diffForHumans() }}
                </p>
            @else
                <div class="form-group date">
                    <label for="deadline">Deadline:</label>
                    <input class="form-control" type="text" name="newDeadline" value="{{$task->deadline ? $task->deadline->format('d/m/yy') : ''}}"/>
                    <span class="add-on"></span>
                </div>
            @endif

            <button type="submit" class="btn btn-primary col-1">Update</button>

        </form>


    </div>
</div>
@endsection

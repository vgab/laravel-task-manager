<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model
{
    use SoftDeletes;

    protected $fillable = ['title','body','important','deadline', 'user_id'];

    protected $dates = ['completed_at', 'deadline', 'deleted_at'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function link()
    {
        return route('task.show', $this);
    }

}

<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Gate;

class TaskController extends Controller
{

    public function index()
    {
        $tasks = collect(Auth::user()->tasks);
        $todos = collect();
        $completed = collect();

        $tasks->each(function ($item, $key) use ($todos,$completed) {

            if($item->completed_at != null)
                $completed->push($item);
            else
                $todos->push($item);
        });

        return view('home',[
            'todos' => $todos->all(),
            'completed' => $completed->all()
        ]);
    }

    public function indexDeleted()
    {
        $tasks = Auth::user()->deletedTasks;

        return view('index-deleted',[
            'deletedTasks' => $tasks
        ]);
    }


    public function store(Request $request)
    {

        request()->validate([
            "title" => "required"
        ]);

        Task::create([
            'title' => $request->title,
            'body' => $request->body,
            'important' => ($request->important) ? 1 : 0,
            'deadline' => $request->deadline ? Carbon::createFromFormat('d/m/Y', $request->deadline) : null,
            'user_id' => Auth::id()
        ]);

        return redirect(route('home'));
    }


    public function show(Task $task)
    {
        if (Gate::denies('see-task', $task)) {
            abort(404);
        }

        return view('show',[
            'task' => $task
        ]);
    }


    public function update(Request $request, Task $task)
    {
        if (Gate::denies('see-task', $task)) {
            abort(401);
        }

        request()->validate([
            "newTitle" => "required"
        ]);

        $task->update([
            'title' => $request->newTitle,
            'body' => $request->newBody,
            'deadline' => $request->newDeadline ? Carbon::createFromFormat('d/m/Y', $request->newDeadline) : null,
            'important' => ($request->newImportant == "on") ? 1 : 0,
        ]);

        return redirect()->back();
    }

    public function toggleComplete(Task $task)
    {
        $task->completed_at = ($task->completed_at == null) ? now() : null;
        $task->save();

        return redirect()->back();
    }

    /** DELETION METHODS **/

    //soft-delete task
    //if it has already been soft-deleted it is removed permanently
    public function destroy($id)
    {
        $task = Task::withTrashed()->find($id);
        if($task)
            if($task->trashed())
            {
                $task->forceDelete();
            }
            else
            {
                $task->delete();
                return redirect(route('home'));
            }
        return redirect()->back();
    }

    //restore a soft-deleted task
    public function restore($id)
    {
        Task::onlyTrashed()->find($id)->restore();
        return redirect(route('index.deleted'));
    }

    //retrieve all the user's soft deleted tasks and remove them permanently
    public function emptyBin()
    {
        Auth::user()->deletedTasks()->forceDelete();
        return redirect(route('home'));
    }


}

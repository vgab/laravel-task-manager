## Project download steps

After cloning the repository you should run these commands:

1. Install composer dependencies - composer install
2. Install node dependencies - npm install
3. Generate application key - php artisan key:generate
4. Run migration - php artisan migrate
5. Run webpack - npm run watch-poll

## .env file setup

The .env.example file should be renamed to .env. You can customize all the attributes but the main ones are these:

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=task-manager
DB_USERNAME=homestead
DB_PASSWORD=secret
```

Edit these as you like.
